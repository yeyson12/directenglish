<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::resource('users', UserController::class)->only(['index','store','edit','update','destroy'])->names('users');
Route::resource('products', ProductController::class)->only(['index','store','edit','update','destroy'])->names('products');

Route::get('change_status/users/{user}', 'App\Http\Controllers\UserController@change_status_user')->name('change.status.user');
Route::get('change_status/products/{product}', 'App\Http\Controllers\ProductController@change_status_product')->name('change.status.product');

