<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $users = User::get();
        return view('users', compact('users'));
        // return $users;
    }
    public function edit(User $user)
    {
        return view('useredit', compact('user'));
    }
    public function store(Request $request)
    {
        $user = User::create($request->all());
        return redirect()->route('users.index')->with('info', 'Usuario creado con exito.');
    }
    public function update(Request $request, User $user)
    {
        $user->update($request->all());
        return redirect()->route('users.index')->with('info', 'Usuario editado con exito.');
    }
    public function change_status_user(User $user)
    {
        if ($user->deleted == 1) {
            $user->update(['deleted'=>0]);
            return redirect()->route('users.index')->with('info', 'Usuario desactivado con exito.');
        } else {
            $user->update(['deleted'=>1]);
            return redirect()->route('users.index')->with('info', 'Usuario activado con exito.');
        } 
    }
}
