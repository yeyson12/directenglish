<?php

namespace App\Http\Controllers;

use App\Models\Products;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Products::get();
        return view('products', compact('products'));
        // return $products;
    }
    public function edit(Products $product)
    {
        return view('productedit', compact('product'));
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'price' => 'required',
            'quantity' => 'required',
        ]);
        $product = Products::create($request->all());
        return redirect()->route('products.index')->with('info', 'Producto creado con exito.');
    }
    public function update(Request $request, Products $product)
    {
        $request->validate([
            'name' => 'required',
            'price' => 'required',
            'quantity' => 'required',
        ]);
        $product->update($request->all());
        return redirect()->route('products.index')->with('info', 'Producto editado con exito.');
    }
    public function change_status_product(Products $product)
    {
        if ($product->deleted == 1) {
            $product->update(['deleted'=>0]);
            return redirect()->route('products.index')->with('info', 'Producto desactivado con exito.');
        } else {
            $product->update(['deleted'=>1]);
            return redirect()->route('products.index')->with('info', 'Producto activado con exito.');
        } 
    }
}
