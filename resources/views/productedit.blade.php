 @extends('index')

 @section('title','Edición de producto')

 @section('styles')
 @endsection

 @section('content')
 <div class="content-wrapper">
 	<div class="page-header">
 		<h3 class="page-title">
 			Productos
 		</h3>
 		<nav aria-label="breadcrumb">
 			<ol class="breadcrumb breadcrumb-custom">
 				<li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
 				<li class="breadcrumb-item" aria-current="page"><a href="{{ route('products.index') }}">Productos</a></li>
 				<li class="breadcrumb-item active" aria-current="page">Editar</li>
 			</ol>
 		</nav>
 	</div>
 	<div class="card">
 		<div class="card-body">
 			<div class="row">
 				<div class="col-lg-12 grid-margin">
 					@if(session('info'))
 					<div class="alert alert-success">
 						<strong>{{session('info')}}</strong>
 					</div>
 					@endif
 					@error(session('name'))
 					<div class="alert alert-danger ">
 						<strong>{{ $message }}</strong>
 					</div>
 					@enderror
 					<h4 class="card-title">Datos del producto</h4>
 					{!! Form::model($product, ['route' => ['products.update', $product], 'method' => 'put']) !!}
 					<fieldset>
 						<div class="row">
 							<div class="col-4">	
 								<div class="form-group">
 									<label for="sku">Sku</label>
 									<input value="{{ $product->sku }}" id="sku" class="form-control" name="sku" type="text">
 								</div>
 							</div>
 							<div class="col-4">	
 								<div class="form-group">
 									<label for="name">Nombre</label>
 									<input required value="{{ $product->name }}" id="name" class="form-control" name="name" type="text">
 								</div>
 							</div>
 							<div class="col-4">	
 								<div class="form-group">
 									<label for="price">Precio</label>
 									<input required step="0.01" value="{{ $product->price }}" id="price" class="form-control" name="price" type="number">
 								</div>
 							</div>
 							<div class="col-4">	
 								<div class="form-group">
 									<label for="quantity">Cantidad</label>
 									<input required value="{{ $product->quantity }}" id="quantity" class="form-control" name="quantity" type="number">
 								</div>
 							</div>
 							<div class="col-4">	
 								<div class="form-group">
 									<label for="description">Descripción</label>
 									<input  value="{{ $product->description }}" id="description" class="form-control" name="description" type="text">
 								</div>
 							</div>
 							<div class="col-4">
				                <div class="form-group">
				                  <label for="deleted">Estatus</label>
				                  <select name="deleted" class="form-control" >
				                    <option value="1"  @if($product->deleted == '1') selected @endif>Activo</option>
				                    <option value="0"  @if($product->deleted == '0') selected @endif>Inactivo</option>
				                  </select>
				                </div>
				            </div>
 						</div>
 						{!! Form::submit('Editar', ['class' => 'btn btn-primary']) !!}
 					</fieldset>
 					{!! Form::close() !!}
 				</div>
 			</div>
 		</div>
 	</div>
 	@endsection

 	@section('scripts')
 	<script src="{{asset('template/js/form-validation.js')}}"></script>
 	<script src="{{asset('template/js/bt-maxLength.js')}}"></script>
 	@endsection