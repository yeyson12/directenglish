<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item nav-profile">
      <div class="nav-link">
        <div class="profile-image">
        </div>
        <div class="profile-name">
          <p class="name">
            Usuario
          </p>
        </div>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ route('users.index') }}">
        <i class="far fa-user menu-icon"></i>
        <span class="menu-title">Usuarios</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ route('products.index') }}">
        <i class="fa fa-address-book menu-icon"></i>
        <span class="menu-title">Productos</span>
      </a>
    </li>
  </ul>
</nav>