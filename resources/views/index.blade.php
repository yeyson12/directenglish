<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>@yield('title')</title>
  <link rel="stylesheet" href="{{asset('template/css/iconfonts/font-awesome/css/all.min.css')}}">
  <link rel="stylesheet" href="{{asset('template/css/vendor.bundle.base.css')}}">
  <link rel="stylesheet" href="{{asset('template/css/vendor.bundle.addons.css')}}">
  <link rel="stylesheet" href="{{asset('template/css/style.css')}}">
  <link rel="shortcut icon" href="" />
  @yield('styles')
</head>
<body class="sidebar-fixed">
  <div class="container-scroller">
    @include('layouts.header')
    <div class="container-fluid page-body-wrapper">
     @include('layouts.sidebar')
     <div class="main-panel">
      @yield('content')
      @include('layouts.footer')
    </div>
  </div>
</div>
<script src="{{asset('template/js/vendor.bundle.base.js')}}"></script>
<script src="{{asset('template/js/vendor.bundle.addons.js')}}"></script>
<script src="{{asset('template/js/off-canvas.js')}}"></script>
<script src="{{asset('template/js/hoverable-collapse.js')}}"></script>
<script src="{{asset('template/js/misc.js')}}"></script>
<script src="{{asset('template/js/settings.js')}}"></script>
<script src="{{asset('template/js/todolist.js')}}"></script>
<script src="{{asset('template/js/dashboard.js')}}"></script>
@yield('scripts')

</body>
</html>