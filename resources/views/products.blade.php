@extends('index')

@section('title','Productos')

@section('styles')
@endsection

@section('content')
<div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title">
      Productos
    </h3>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        @can('admin.home')
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
        @endcan
        <li class="breadcrumb-item active" aria-current="page">Productos</li>
      </ol>
    </nav>
  </div>
  <div class="card">
    <div class="card-body">
      @if(session('info'))
      <div class="alert alert-success">
        <strong>{{session('info')}}</strong>
      </div>
      @endif
      @error(session('name'))
      <div class="alert alert-danger ">
        <strong>{{ $message }}</strong>
      </div>
      @enderror
      <div class="d-flex justify-content-between">
        <h4 class="card-title">Productos</h4>
        <a href="" class="nav-link" type="button" data-toggle="modal" data-target="#exampleModal-4" data-whatever="@mdo">
          <span class="btn btn-outline-primary btn-icon-text btn-sm">Crear</span>
        </a>
        <div class="nav-link modal fade" id="exampleModal-4" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="ModalLabel">Nuevo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              {!! Form::open(['route' => 'products.store']) !!}
              <div class="modal-body">
                <div class="form-group">
                  {!! Form::label('sku', 'SKU') !!}
                  {!! Form::text('sku', null, ['class' => 'form-control', 'placeholder' => 'Ingrese el sku']) !!}
                </div>
                <div class="form-group">
                  {!! Form::label('name', 'Nombre') !!}
                  {!! Form::text('name', null, ['required' => 'true','class' => 'form-control', 'placeholder' => 'Ingrese el nombre']) !!}
                </div>
                <div class="form-group">
                  {!! Form::label('quantity', 'Cantidad') !!}
                  {!! Form::number('quantity', null, ['required' => 'true','class' => 'form-control', 'placeholder' => 'Ingrese la cantidad']) !!}
                </div>
                <div class="form-group">
                  {!! Form::label('price', 'Precio') !!}
                  {!! Form::number('price', null, ['required' => 'true','step' => '0.01', 'class' => 'form-control', 'placeholder' => 'Ingrese el precio']) !!}
                </div>
                <div class="form-group">
                  {!! Form::label('description', 'Descipción') !!}
                  {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Ingrese la descipción']) !!}
                </div>
                <div class="form-group">
                  {!! Form::label('deleted', 'Estatus') !!}
                  <select name="deleted" class="form-control" required>
                    <option value="">Seleccione...</option>
                    <option value="1">Activo</option>
                    <option value="0">Inactivo</option>
                  </select>
                </div>
              </div>
              <div class="modal-footer">
                {!! Form::submit('Crear', ['class' => 'btn btn-success']) !!}
                <button type="button" class="btn btn-light" data-dismiss="modal">Cerrar</button>
              </div>
              {!! Form::close() !!}
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <div class="table-responsive">
            <table id="order-listing" class="table">
              <thead>
                <tr>
                  <th>Sku</th>
                  <th>Nombre</th>
                  <th>Cantidad</th>
                  <th>Precio</th>
                  <th>Descipción</th>
                  <th>Eliminado</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
                @foreach($products as $key => $product)
                <tr>
                  <td>{{$product->sku}}</td>
                  <td>{{$product->name}}</td>
                  <td>{{$product->quantity}}</td>
                  <td>{{$product->price}}</td>
                  <td>{{$product->description}}</td>
                    @if ($product->deleted == 1)
                    <td>
                        <a class="jsgrid-button btn btn-success" href="{{route('change.status.product', $product)}}" title="Desactivar">
                            <!-- Activa --> <i class="fas fa-check"></i>
                        </a>
                    </td>
                    @else
                    <td>
                        <a class="jsgrid-button btn btn-danger" href="{{route('change.status.product', $product)}}" title="Activar">
                            <!-- Inactiva --> <i class="fas fa-times"></i>
                        </a>
                    </td>
                    @endif
                  <td>         
                    <a class="btn btn-outline-info" href="{{ route('products.edit',$product) }}" title="Editar">
                      <i class="far fa-edit"></i>
                    </a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="{{asset('template/js/data-table.js')}}"></script>
@endsection