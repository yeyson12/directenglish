 @extends('index')

 @section('title','Edición de Usuario')

 @section('styles')
 @endsection

 @section('content')
 <div class="content-wrapper">
  <div class="page-header">
    <h3 class="page-title">
      Usuarios
    </h3>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb breadcrumb-custom">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
        <li class="breadcrumb-item" aria-current="page"><a href="{{ route('users.index') }}">Usuarios</a></li>
        <li class="breadcrumb-item active" aria-current="page">Editar</li>
      </ol>
    </nav>
  </div>
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-lg-12 grid-margin">
          @if(session('info'))
          <div class="alert alert-success">
            <strong>{{session('info')}}</strong>
          </div>
          @endif
          @error(session('name'))
          <div class="alert alert-danger ">
            <strong>{{ $message }}</strong>
          </div>
          @enderror
          <h4 class="card-title">Datos del Usuario</h4>
          {!! Form::model($user, ['route' => ['users.update', $user], 'method' => 'put']) !!}
          <fieldset>
            <div class="row">
              <div class="col-4"> 
                <div class="form-group">
                  <label for="name">Nombre</label>
                  <input required value="{{ $user->name }}" id="name" class="form-control" name="name" type="text">
                </div>
              </div>
              <div class="col-4"> 
                <div class="form-group">
                  <label for="email">Correo</label>
                  <input required value="{{ $user->email }}" id="email" class="form-control" name="email" type="email">
                </div>
              </div>
              <div class="col-4"> 
                <div class="form-group">
                  <label for="phone">Teléfono</label>
                  <input required value="{{ $user->phone }}" id="phone" class="form-control" name="phone" type="text">
                </div>
              </div>
              <div class="col-4"> 
                <div class="form-group">
                  <label for="username">Usuario</label>
                  <input required value="{{ $user->username }}" id="username" class="form-control" name="username" type="text">
                </div>
              </div>
              <div class="col-4"> 
                <div class="form-group">
                  <label for="dateofbirth">Fecha de nacimiento </label>
                  <input required value="{{ $user->dateofbirth }}" id="dateofbirth" class="form-control" name="dateofbirth" type="date">
                </div>
              </div>
              <div class="col-4"> 
                <div class="form-group">
                  <label for="state">Estado </label>
                  <input required value="{{ $user->state }}" id="state" class="form-control" name="state" type="text">
                </div>
              </div>
              <div class="col-4">
                <div class="form-group">
                  <label for="deleted">Estatus</label>
                  <select name="deleted" class="form-control" >
                    <option value="1"  @if($user->deleted == '1') selected @endif>Activo</option>
                    <option value="0"  @if($user->deleted == '0') selected @endif>Inactivo</option>
                  </select>
                </div>
              </div>
            </div>
            {!! Form::submit('Editar', ['class' => 'btn btn-primary']) !!}
          </fieldset>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
  @endsection

  @section('scripts')
  <script src="{{asset('template/js/form-validation.js')}}"></script>
  <script src="{{asset('template/js/bt-maxLength.js')}}"></script>
  @endsection